package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    List<Project> findAll(Sort sort);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}