package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.DescriptionEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        if (StringUtils.isBlank(description)) throw new DescriptionEmptyException();
        return projectRepository.create(name.trim(), description.trim());
    }

    @Override
    public Project create(final String name) {
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        return projectRepository.create(name.trim());
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Project> comparator = sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public Project findOneById(final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return projectRepository.findOneById(id.trim());
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return projectRepository.removeById(id.trim());
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public int getSize() {
        return projectRepository.getSize();
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Project project = findOneById(id.trim());
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name.trim());
        project.setDescription(description.trim());
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name.trim());
        project.setDescription(description.trim());
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        final Project project = findOneById(id.trim());
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}